/**
 * Includes
 */
#include <iostream>   // cout, cin
#include <algorithm>

#include "includes/headers/constants.h"
#include "includes/headers/globals.h"





/**
 * Custom functions
 */
void searchForCustomers ()
{
    std::string query = "";

    do {
        //clears screen
        glb::clear();

        //print search query
        glb::print("Results for search query: " + query);

        // search for customers
        glb::customers->search([] (auto customer) {
            customer->display(); // display all matches
        }, query);

        //get input
        query = glb::getInput("Search query (uppercase 'Q' to exit)", 1, MAX_LENGTH_NAME);
    } while (query != "Q");
}

void addItemsToMenu (std::function<void()> callback, std::string state, const int timeout)
{
    glb::items->load(state);
    glb::items->loop([&callback, &timeout] (auto item, const int i) {
        glb::addMenuEntry(
                {std::to_string(i), glb::customers->state()},
                {item->title(), PUBLIC, ADMIN, callback, glb::enum_item, timeout} // timeout == 1
        );
    });
}

// Discovers all completed auctions and sets them as purchases/sales in users file
void updateAuctions()
{
    std::string catID;
    std::ifstream stream ("data/categories.txt");

    // Get all existing category IDs
    while (stream.ignore() && stream >> catID && stream.ignore(200, '\n'))
    {
        // If the category is a sub-category
        if (catID.length() > 1)
        {
            Items items = Items();
            items.load(catID);
            items.updateAuctions();
        }
    }
}

void updateLoop ()
{
    /**
     * Whenever a update / notification is present for the user,
     *  it gets automatically displayed next to their username in the menu.
     */
    { //private scope
        //do something to check for a new user update!!
        // use customers->session()->addUpdateNotification(int i = 1)
        // or customers->session()->removeUpdateNotification(int i = 1)
        // to either add more notifications or remove.
        if (glb::customers->authenticated() && glb::customers->session()->newUpdate())
        {
            glb::userUpdates = "[" + std::to_string(glb::customers->session()->newUpdate()) + "]";
        }
        updateAuctions();
    }
}



/**
 * First project submit
 */
void registerCustomer ()        // KR
{
    glb::clear();

    /**
     * checks if user is already logged in nad isn't an admin
     */
    if (glb::customers->session()->getAuthorizationLevel() != ADMIN) {
        glb::print("You are already logged in!");
        return;
    }

    /**
     * register form message.
     */
    glb::print(MENU_SIDES + " Registration " + MENU_SIDES);
    glb::print();
    glb::print("Please enter a username below.");
    glb::print("Be aware that the username is case sensitive!");
    glb::print();


    glb::customers->create();
}
void loginCustomer ()           // KL
{
    glb::clear();

    /**
     * checks if user is already logged in
     */
    if (glb::customers->authenticated()) {
        glb::print("You are already logged in!");
        return;
    }

    /**
     * login form message.
     */
    glb::print(MENU_SIDES + " Authentication " + MENU_SIDES);
    glb::print();
    glb::print("Please enter your username and password below.");
    glb::print("Be aware that the username and password are both case sensitive!");
    glb::print();

    /**
     * Get user data from terminal.
     */
    std::string username = glb::getInput("Username");
    std::string password = glb::getInput("Password", MIN_LENGTH_PASS, MAX_LENGTH_PASS);

    /**
     * Pretend to be loading..
     */
    std::cout << "Checking username and password ." << std::flush; glb::fakeLoading();

    /**
     * Checks if username and passwords are correct.
     */
    if (glb::customers->authenticate(username, password)) {
        glb::print("OK");
        glb::print(); // new line
    }
}
void logoutCustomer ()          // KU
{
    glb::customers->deauthenticate();

    /**
     * Pretend to be doing heavy work..
     */
    std::cout << "Logging out" << std::flush; glb::fakeLoading(); glb::print("OK");
}
void displayCustomerItems ()    // KS
{
    glb::customers->session()->displayItems();
}
void setAuthorityLevel ()       // KE
{
    //clears screen
    glb::clear();

    if (glb::customers->session()->getAuthorizationLevel() != ADMIN) {
        std::cerr << "You must be admin!" << std::endl;
        return;
    }

    //get username
    std::string username = glb::getInput("Username", MIN_LENGTH_NAME, MAX_LENGTH_NAME);

    //find customer
    glb::customers->loop([&] (Customer* customer) {
        if (customer->username() == username) {
            username = "";
            customer->display();
            glb::print();//new empty line

            glb::print("Authorization levels:");
            glb::print(std::to_string(CUSTOMER) + " - customer");
            glb::print(std::to_string(ADMIN)    + " - admin");

            int level = glb::getInt("Level", CUSTOMER, ADMIN);
            customer->setAuthorization(level);
            glb::customers->save();
        }
    });

    if (username.length() > 0) {
        std::cerr << "Username was not found in the system" << std::endl;
    }
}
void createCategory ()          // N
{
    auto category = glb::categories->getActiveCategory(glb::customers->state());

    if (category != nullptr)
    {
        category->create();
    }
    else
    {
        glb::categories->create();
    }

    if (!glb::owner_menuStateFree())
    {
        glb::customers->previousState(); //get out of function state
        glb::categorySurfing(); //update menu
    }
}

/**
 * Second / last project submit
 */

/**
 * Lets the user look through categories such as if it was a file system
 */
void glb::categorySurfing () // AS
{
    glb::categories->surf();
}

/**
 * Displays all the auctions listed in the categories folder.
 *  These will always be active auctions.
 */
void displayActiveAuctions () //A
{
    /**
     * Instantiate the items pointer
     */
    glb::items = new Items();

    /**
     * Add each auction that exists in memory to the menu, and use
     *  this lambda closure to handle its functionality.
     */
    addItemsToMenu([] () {
        if (!glb::owner_menuStateFree())
        {
            glb::customers->previousState(2); //get out of function state
        }
    }, glb::customers->state(), 1);

    /**
     * Free it from memory, only a copy of the text is needed
     */
    delete glb::items;
}

/**
 * Displays all available information about a auction / item.
 */
void displayInfoAboutItem () //E
{
    /**
     * Instantiate the items pointer
     */
    glb::items = new Items();

    /**
     * Add each auction that exists in memory to the menu, and use
     *  this lambda closure to handle its functionality.
     */
    addItemsToMenu([] () {
        int id = glb::customers->state().back() - '0';
        glb::items->display(id);

        if (!glb::owner_menuStateFree())
        {
            glb::customers->previousState(2); //get out of function state
        }

        /**
         * Free it from memory
         */
        delete glb::items;
    }, glb::customers->state(), 1);
}

/**
 * Pays for an active auction
 */
void payForAuction ()
{
    glb::customers->payForAuction(glb::customers->session()->username());
}

/**
 * Lets a seller or customer give each other a comment and a rating.
 *
 * Customers can only give feedback when they have paid for the item,
 *  while the seller can give a feedback before payment.
 */
void giveFeedback ()
{
    /**
     * Take ownership of the menu state
     */
    glb::setMenuStateOwnership();


    /**
     * List of all the users the logged in user have had any business with
     */
    std::map<std::string, bool> users;

    /**
     * Load content from the K file
     */
    std::string kFile = "customers/K" + glb::customers->session()->username();
    glb::read(kFile, [&users] (auto& stream) {
        /**
         * File layout
         *
         * <"PAID"/"NOT PAID">  <- id an auction
         * <ItemName>
         * <Seller>             <- to be extracted
         * <FinalPrice>
         */

        /**
         * check for a auction.
         *
         * The First line of each auction is "HAS PAID" or "NOT PAID"
         *  therefore the program check if the line " PAID" exists as this
         *  is unique due to its uppercase value.
         *
         *  the customer must always have paid before the can comment.
         */
        std::string status;
        while (std::getline(stream, status))
        {
            if (status == "HAS PAID")
            {
                break;
            }
        }

        /**
         * Get content
         */
        /* Ignore the item line */
        stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

        std::string seller;
        std::getline(stream, seller);

        /**
         * Only let the customer send feedback if he
         *  or she has actually paid for the item
         */
        if (!seller.empty())
        {
            users.insert(std::pair<std::string, bool>(seller, true));
        }
    });

    /**
     * Load everything from the S file
     */
    std::string sFile = "customers/S" + glb::customers->session()->username();
    glb::read(sFile, [&users] (auto& stream) {
        /**
         * File layout
         *
         * <"PAID"/"NOT PAID">
         * <ItemName>
         * <Buyer>              <- to be extracted
         * <FinalPrice>
         */

        /**
         * check for a auction.
         *
         * The First line of each auction is "HAS PAID" or "NOT PAID"
         *  therefore the program check if the line " PAID" exists as this
         *  is unique due to its uppercase value.
         */
        std::string status;
        while (std::getline(stream, status))
        {
            /**
             * Found a match, end while loop
             */
            if (status.find(" PAID") != std::string::npos)
            {
                break;
            }
        }

        /**
         * Get content
         */
        /* Ignore the item line */
        stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

        std::string customer;
        std::getline(stream, customer);

        /**
         * Only let the customer send feedback if he
         *  or she has actually paid for the item
         */
        if (!customer.empty())
        {
            users.insert(std::pair<std::string, bool>(customer, false));
        }
    });

    /**
     * Add user names to menu
     */
    int index = 0;
    for (const auto& user : users)
    {
        /**
         * Add each user the logged in user have had any business with to the menu.
         */
        glb::addMenuEntry(
                {std::to_string(index++), glb::customers->state()},
                {user.first, CUSTOMER, ADMIN, [user] () {
                    /**
                     * private function pointer for each of the new menu objects.
                     */

                    std::string command;
                    std::string description;
                    std::string file = "data/customers/";

                    if (!user.second) // K customer that wont your auction
                    {
                        command     = "K";
                        description = "Comment on their customer behavior";
                        file        += command + user.first;
                    }
                    else
                    {
                        command     = "S";
                        description = "Comment on your purchase experience";
                        file        += command + glb::customers->session()->username();
                    }
                    file += ".txt";

                    //get out of /KF/i and back to /KF
                    glb::customers->previousState();

                    /**
                     * When the logged in user have selected a seller or customer,
                     *  he is presented with either writing a comment about how they
                     *  were as a salesman or customer. Depending on what is relevant.
                     */
                    glb::addMenuEntry(
                            {command, glb::customers->state()},
                            {description, CUSTOMER, ADMIN, [user, file] () {
                                /**
                                 * private function pointer for each of the new menu objects.
                                 */

                                //add to file
                                std::ofstream stream;
                                stream.open(file, std::ofstream::out | std::ios::app);

                                if (stream.bad()) {
                                    std::cerr << "Unable to read from " + file << std::endl;
                                }
                                else
                                {
                                    stream << std::endl << std::endl;
                                    stream << "FEEDBACK" << std::endl;
                                    stream << user.first << std::endl;
                                    stream << glb::getInput("Write your comment", 0, 180)   << std::endl;
                                    stream << glb::getInput("Set a rating grade", 0, 6)     << std::endl;
                                }

                                //get out of /KF/i and back to /
                                glb::customers->previousState(2);
                            }} // timeout == 1
                    );
                }} // timeout == 1
        );
    }

    /**
     * If the index hasn't incremented, it means there were no data
     *  in the logged in users files.
     */
    if (index == 0)
    {
        glb::print(MENU_SIDES + " NOTHING TO SHOW " + MENU_SIDES);
        glb::print("For won auctions you need to pay before you can comment.");
        glb::print();
        glb::customers->previousState(); //get out of /KF and back into /
    }
}

/**
 * Lets an customer bid on an active auction
 */
void bidOnItem ()
{
    auto category = glb::categories->getActiveCategory(glb::customers->state());

    auto items = Items();
    items.load(category->path());
    items.bid();
}

/**
 * Lets a customer add a new auction to the active category
 */
void addNewItem ()
{
    auto category = glb::categories->getActiveCategory(glb::customers->state());

    auto items = Items();
    items.load(category->path());
    items.create();
}



/**
 * Shows the main menu.
 */
void initiateMenu() {
    std::string selection;

    /**
     * add menu entries!
     *
     * use star (/*) to make it visible at any state later
     */
    /* menu choice,     state,   Description of choice,               min auth,   max auth, void function to call */
    glb::addMenuEntry(
            {"KR", "/"},
            {"Customer registration",                GUEST,      ADMIN, registerCustomer});//
    glb::addMenuEntry(
            {"KL", "/"},
            {"Customer login",                       GUEST,      GUEST, loginCustomer});//
    glb::addMenuEntry(
            {"KU", "/"},
            {"Customer logout",                      CUSTOMER,   ADMIN, logoutCustomer});//
    glb::addMenuEntry(
            {"KS", "/"},
            {"Display my items",                     CUSTOMER,   ADMIN, displayCustomerItems});//
    glb::addMenuEntry(
            {"KB", "/"},
            {"Pay for a won auction / item",         CUSTOMER,   ADMIN, payForAuction});
    glb::addMenuEntry(
            {"KF", "/"},
            {"Give another user feedback",           CUSTOMER,   ADMIN, giveFeedback});
    glb::addMenuEntry(
            {"KE", "/"},
            {"Update customer authorization level",  ADMIN,      ADMIN, nullptr, glb::enum_path});//
    glb::addMenuEntry(
            {"S", "/KE"},
            {"Search among users",                   ADMIN,      ADMIN, searchForCustomers});//
    glb::addMenuEntry(
            {"A", "/KE"},
            {"Set authorization level for a user",   ADMIN,      ADMIN, setAuthorityLevel});//
    glb::addMenuEntry(
            {"N", "/AS*"},
            {"Create a new category",                ADMIN,      ADMIN, createCategory});
    glb::addMenuEntry(
            {"AS", "/"},
            {"Go through the categories",            GUEST,      ADMIN, glb::categorySurfing, glb::enum_category}); //
    glb::addMenuEntry(
            {"A", "/AS/*"},
            {"View only active auctions",            GUEST,      ADMIN, displayActiveAuctions});
    glb::addMenuEntry(
            {"E", "/AS/*"},
            {"View details about an item",           GUEST,      ADMIN, displayInfoAboutItem});
    glb::addMenuEntry(
            {"L", "/AS/*"},
            {"Add a new auction",                    CUSTOMER,   ADMIN, addNewItem});
    glb::addMenuEntry(
            {"B", "/AS/*"},
            {"Submit an offer",                      CUSTOMER,   ADMIN, bidOnItem});
    glb::addMenuEntry(
            {"Q", "/*"},
            {"Quit current program / go back",       GUEST,      ADMIN});

    /**
     * Start the program loop.
     */
    do
    {
        /**
         * Place for functions to get called on each menu iteration.
         */
        updateLoop();

        /**
         * local varaibles for less typing later
         */
        bool authenticated = glb::customers->authenticated();
        int authorization  = authenticated ? glb::customers->session()->getAuthorizationLevel() : GUEST;
        std::map<std::string, std::function<void()>> activeMenu;
        std::string cState = glb::customers->state();


        /**
         * After everything has been loaded into the active menu
         *  check for out of date menu options to be removed.
         *
         * if timeout == -1, it lasts forever
         */
        for (auto it = glb::menu.begin(); it != glb::menu.end(); )
        {
            if (it->second.timeout > 0)
            {
                it->second.timeout -= 1;
                ++it;
            }
            else if (it->second.timeout == 0)
            {
                it = glb::menu.erase(it);
            }
            else
            {
                ++it;
            }
        }

        /**
         * Display that a customer is logged in
         *  if his / her session is successfully set.
         */
        if (authenticated)
        {
            glb::print("Logged in as: " + glb::customers->session()->username() + glb::userUpdates);
        }

        /**
         * Print the menu, with command options.
         *
         * ROOT_STATE == /
         */
        glb::print(MENU_SIDES + " MENU " + MENU_SIDES);

        for (auto const& i : glb::menu)
        {
            std::string state   = i.first.state;
            int minAuth         = i.second.minAuth;
            int maxAuth         = i.second.maxAuth;

            /**
             * If the current entry does not follow the rules for being displayed,
             *  we ignore the rest of the round and go to the next entry.
             */
            bool match      = true;
            bool wildcard   = true;
            bool authorized = true;

            /**
             * Wildcard check
             */
            std::size_t pos = state.find('*');
            if (pos == std::string::npos)
            {
                wildcard = false;
            }

            /**
             * State checks.
             *
             * Wildcard routes lets the menu option be displayed in any
             *  category that exists after a certain point.
             */
            std::string stateSub = state.substr(0, state.length() - 1);
            if (wildcard && cState.find(stateSub) == std::string::npos)
            {
                match = false;
            }
            else if (!wildcard && state != glb::customers->state())
            {
                match = false;
            }

            /**
             * Authorization checks
             *
             * 1. Displays menu menu for only guests!
             * 2. Make sure customers that does not have enough security
             *      clearance can't access the menu entry.
             * 3. For functions we want to hide when a customer has a
             *      certain level.
             *      Hides eg. send a comment to an admin (not implemented)
             */
            if (minAuth > authorization || maxAuth < authorization)
            {
                authorized = false;
            }

            /**
             * Verify checks, skip the rest of the loop if
             *  there were any mismatch.
             */
            if (!match || !authorized)
            {
                continue;
            }

            /**
             * prints out the menu entry: COMMAND - description
             *  and also separates the different menu types with a symbol.
             */
            std::string menuType = "\t";
            switch (i.second.race)
            {
                case glb::enum_item:     menuType += "#"; break;
                case glb::enum_category: menuType += "/"; break;
                case glb::enum_path:     menuType += "+"; break;
                default:                 menuType += "~"; break; // function
            }
            menuType += " ";
            glb::print(i.first.command + menuType + i.second.description);

            /**
             * let the program know which commands were displayed.
             */
            activeMenu.insert({i.first.command, i.second.fptr});
        }



        /**
         * Retrieves the selected option as a string by the customer.
         *  The string is then converted to an enum, and
         */
        glb::print();
        glb::print("Current scope: " + glb::customers->state());
        selection = glb::getInput("Menu choice", 1, 2, true);   // Get command from user. Lengths; min: 1, max: 2. Uppercase.

        /**
         * When the returned position isn't set to the end()[index value]
         *  there was a match.
         */
        if (activeMenu.find(selection) != activeMenu.end()) {
            /**
             * Check if the script is asked to be terminated.
             * Skip rest of the code if so, no need to run it then.
             */
            if (selection == "Q" && glb::customers->state() == ROOT_STATE)
            {
                break;
            }

            /**
             * Apply the state change
             */
            glb::customers->updateState(selection); // state when within function

            /**
             * menu that doesnt have a function pointer, are usually just states for another menu.
             *  eg. KE, rolling through categories, etc.
             *
             *  These are therefor not being called as they'll give an error.
             *  However whenever a function is called, you always want to go back to the previous
             *   menu entry.
             */
            if (activeMenu.at(selection) != nullptr) {
                (activeMenu.at(selection))(); //call function pointer

                /**
                 * Watcher to update the menuState if the function activating it
                 *  has been exited / ended.
                 */
                glb::owner_menuStateChanged();

                /**
                 * Some functions might not want to go back to the previous state.
                 *  Therefor a fake mutex is created to keep the customers->previousState
                 *  from being called. This might be ugly, but atm it works.
                 *
                 *  ofc there aren't any async action here so its no need to change the
                 *   mutex state after usage, except in classes that needs to
                 *   populate the customersState.
                 */
                if (glb::owner_menuStateFree()) {
                    glb::customers->previousState(); //global mutex for dynamic menu? YES PLEASE
                }
            }
        } else {
            glb::print("Unable to find menu entry '" + selection + "'");
        }

    } while (true);
}



/**
 * Main program
 */
int main()
{
    /**
     * Initiate the globals lists.
     *
     * The lists aren't stored in the main.cpp but rather the globals.*
     *  for easier access within the other main classes.
     */
    glb::customers     = new Customers();                        //
    glb::categories    = new Categories();                       //

    /**
     * Here the start of the main program is initiated.
     */
    //updateAuctions();
    initiateMenu();                                         // Start menu

    /**
     * Destroy objects.
     * This also allows for code to be executed inside the destroy method.
     *  If they aren't delete before the OS frees the memory automatically,
     *  the code inside the destroy method will not be called.
     *
     * In this case, a save method is called within each destructor to save
     *  any unsaved changes.
     */
    delete glb::customers;
    delete glb::categories;

    /**
     * Successfully exits the program at end.
     *  status = 0.
     */
    return EXIT_SUCCESS;
}