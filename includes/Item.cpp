
#include "headers/Item.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include "headers/Timer4.h"
#include "headers/Bid.h"
#include "headers/globals.h"

/**
 * Initializes the object from a file stream
 */
Item::Item(std::ifstream& stream, int id)
        :NumElement (id)
{
    std::string buffer;
    _bids = new List(Sorted);

        // Remove newline character from last ">>"
    stream.ignore();
        // Read every line in the item object
    std::getline(stream, _seller);

    std::getline(stream, _title);

    std::getline(stream, _description);

    std::getline(stream, buffer);
    _start   = std::stol(buffer);

    std::getline(stream, buffer);
    _end     = std::stol(buffer);

    std::getline(stream, buffer);
    _startingPrice = std::stoi(buffer);

    std::getline(stream, buffer);
    _bidIncrement = std::stoi(buffer);

    std::getline(stream, buffer);
    _porto   = std::stoi(buffer);

        // If the next line is not empty, this means there are bids
    while (std::getline(stream, buffer) && !buffer.empty())
    {
        std::string name = buffer;

        std::getline(stream, buffer);
        int num = std::stoi(buffer);
            // Create and add new bid instance to the list
        _bids->add(new Bid(stream, name, num));
    }
}

/**
 * Initializes object from user-specified data
 */
Item::Item(int id, std::string usr, std::string title, std::string desc, int startprice, int bidinc, int porto, long int start, long int end)
        :NumElement (id)
{
    _seller = usr;
    _title = title;
    _description = desc;
    _startingPrice = startprice;
    _bidIncrement = bidinc;
    _porto = porto;
    _start = start;
    _end = end;

    _bids = new List(Sorted);
}

/**
 * Removes the pointer _bids
 */
Item::~Item()
{
    delete _bids;
}

/**
 * Returns true if parameter and object ID compare equal
 */
bool Item::compare(int id)
{
    return id == number;
}

/**
 * Returns true if parameter string and item name compares equal
 */
bool Item::compare(std::string str)
{
    return str == _title;
}

/**
 * Displays information about this item
 */
void Item::display()
{
    std::string timestamp;

    glb::print("--------ITEM ID: " + std::to_string(number) + "--------");
    glb::print("Name: " + _title);
    glb::print("Description: " + _description);
    glb::print("Seller: " + _seller);
    timestamp = std::to_string(_start);
            // Format the time and date output
    std::cout << "Time period: " << std::setw(7) << "FROM " << timestamp.substr(0, 4) << ", " << timestamp.substr(4, 2)
              << '.' << timestamp.substr(6, 2) << ", " << timestamp.substr(8, 2) << ':' << timestamp.substr(10, 2);
    timestamp = std::to_string(_end);
    std::cout << std::endl << std::setw(18) <<  "TO " << timestamp.substr(0, 4) << ", " << timestamp.substr(4, 2) << '.'
    << timestamp.substr(6, 2) << ", " << timestamp.substr(8, 2) << ':' << timestamp.substr(10, 2) << std::endl;
}

/**
 * Displays every detail about this item
 */
void Item::displayDetails()
{
    this->display();
    if (!_bids->isEmpty())
    {
        _bids->displayList();
    }
}

/**
 * Lets a logged in user place a bid on this auction
 */
void Item::bid(std::string usr)
{
    int bidamount;
    int minbid;
    long int timestamp;
    Timer4 timer4;
    int d, m, y, h, mi;
        // Check if user is the auction creator
    if (usr == _seller)
    {
        glb::print("Error: you cannot bid on your own auction");
        return;
    }

        // First bid: set minimum bid to start price + increment
    if (_bids->isEmpty()) minbid = _startingPrice + _bidIncrement;
        // Not first bid: set minimum bid to last bid + increment
    else
    {
        Bid* lastbid = (Bid*) _bids->removeNo(_bids->noOfElements());
        minbid = lastbid->returnValue() + _bidIncrement;
        _bids->add(lastbid);
    }
        // Get bid amount from user
    bidamount = glb::getInt("Bid amount: ", minbid, MAX_AUCTION_AMOUNT);
        // Get and format timestamp
    timer4.get(d, m, y, h, mi);
    timestamp = glb::formatTime(y, m, d, h, mi);
        // Create and add a new Bid instance to the list
    Bid* bid = new Bid(bidamount, usr, timestamp);
    _bids->add(bid);
}

/**
 * Checks if this auction has ended and if so
 * customers files are appended
 */
void Item::updateAuctions(long int timestamp, bool& ended)
{
        // Auction ended and there are bids?
    if (timestamp > _end && !_bids->isEmpty())
    {
        std::string winner;
        std::string seller = _seller;
        int finalprice;

            // Remove last bidder (which contains the winner)
        Bid* bid = (Bid*) _bids->removeNo(_bids->noOfElements());
        winner = bid->returnBidder();
        finalprice = bid->returnValue();

            // Append item to winner and seller customer files
        std::ofstream out;
        out.open("data/customers/K" + winner + ".txt", std::ios::app);
        out << "NOT PAID" << std::endl << std::endl;
        out << _title << std::endl;
        out << seller << std::endl;
        out << finalprice << std::endl;
        out.close();
        out.open("data/customers/S" + seller + ".txt", std::ios::app);
        out << "NOT PAID" << std::endl << std::endl;
        out << _title << std::endl;
        out << winner << std::endl;
        out << finalprice << std::endl;

        ended = true;
    }
        // Auction ended and there are no bids?
    else
    {
        ended = timestamp > _end && _bids->isEmpty();
    }
}

/**
 * Writes all item data to an output stream
 */
void Item::save(std::ofstream& stream)
{
    stream << number << std::endl
    << _seller << std::endl
    << _title << std::endl
    << _description << std::endl
    << _start << std::endl
    << _end << std::endl
    << _startingPrice << std::endl
    << _bidIncrement << std::endl
    << _porto << std::endl;
    for (int i = LISTTOOLSTART; i <= _bids->noOfElements(); i++) // RemoveNo starts at 1
    {
        Bid* bid = (Bid*) _bids->removeNo(i);
        bid->save(stream);
        _bids->add(bid);
    }
    stream << std::endl;
}

/**
 * Returns this items title
 */
std::string Item::title() const
{
    return _title;
}