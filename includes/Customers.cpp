/**
 * Header file
 */
#include "headers/Customers.h"

/**
 * Includes
 */
#include "headers/globals.h"

#include <iostream>



/**
 * Private methods
 */

/**
 * Checks if the list is empty or if an item exists within.
 * A response message with its findings are shown.
 */
void Customers::_empty() const
{
    if (_customers->isEmpty()) {
        std::cout << "WARNING 204: No content found! \n\n";
        return;
    }
}
void Customers::_exists(int& i) const
{
    if (i < LISTTOOLSTART) {
        i = LISTTOOLSTART;
    }

    if (!_customers->inList(i)) { //TODO: must be char []
        std::cout << "WARNING 404: User was not found! \n\n";
        return;
    }
}

void Customers::_exists(std::string username) const
{
    if (!_customers->inList(glb::strToChar(username))) {
        std::cout << "WARNING 404: User was not found! \n\n";
        return;
    }
}

/**
 * Loads all customer data from file
 */
void Customers::_load ()
{
    std::ifstream stream;
    if (!glb::readable(_file, stream)) {
        return;
    }

    while (stream.good()) {
        std::string username;
        std::getline(stream, username);

        if (username.length() == 0) {
            continue; //if stream is just a new line, the string will have a length of 0.
                        //its therefor skipped. Otherwise invalid userdata will be loaded.
        }

        Customer * customer = new Customer(username, stream);
        _customers->add(customer);
    }
}


/**
 * Public methods
 */

/**
 * Constructor create a new sorted List instance
 */
Customers::Customers()
{
    _customers  = new List(Sorted);
    _session    = nullptr;
    _file       = "customers"; // /data/customers

    _state      = "/";

    this->_load(); //load customers from file
}

Customers::~Customers()
{
    /**
     * Calls the list destructor.
     *
     * "delete _session" is not used as it's a pointer to
     *  and object within _customers. No need for double deletion.
     *
     *  _file, does not need to be deleted as this isn't a pointer.
     */
    delete _customers;      // then delete the users!
}

/**
 * @return boolean; true for logged in, false for logged out.
 */
bool Customers::authenticated() const
{
    return (_session != nullptr);
}


/**
 * @return Customer pointer for session, or nullptr if logged out.
 */
Customer * Customers::session () const
{
    return _session;
}

bool Customers::authenticate(std::string username, std::string password)
{
    /**
     * Customer object that is logging in..
     */
    Customer * session;

    /**
     * Checks if username and passwords are correct.
     */
    session = (Customer *) _customers->remove(glb::strToChar(username));
    if (session == NULL) { // not sure why it returns NULL and not nullptr..
        std::cout << "Wrong username!" << std::endl;
        return false;
    }

    if (!session->authenticate(password)) {
        std::cout << "Wrong password!" << std::endl;
        return false;
    }

    /**
     * get & create user session
     */
    _customers->add(session);   //add user back to list
    _session = session;         //set session

    return true;
}

/**
 * Deauthenticates the active user
 */
void Customers::deauthenticate ()
{
    _session = nullptr;
}

std::string Customers::state () const
{
    return _state;
}

void Customers::previousState(int i)
{
    do
    {
        if (_state == ROOT_STATE) {
            return;
        }

        //remove everything until a backslash is hit, and remove that too
        std::size_t found = _state.find_last_of(ROOT_STATE);
        _state = _state.substr(0, found);

        if (_state.length() == 0) {
            _state = ROOT_STATE;
        }
    }
    while (--i);
}

void Customers::updateState(std::string state)
{
    if (state == "Q") {
        this->previousState();
        return;
    }

    if (_state.length() == 1) {
        _state += state;
    } else {
        _state += ROOT_STATE + state;
    }
}

/**
 * Lets a logged in user pay for all won auctions that
 * are pending payment
 */
void Customers::payForAuction(std::string user)
{
                // Open file in read/write mode
    std::fstream stream("data/customers/K" + user + ".txt", std::ios::in | std::ios::out);
    if (!stream) return;

    std::string title, seller, price, buffer, choice;
    long int currentpos;
    while (std::getline(stream, buffer))
    {           // Get file stream position
        currentpos = int(stream.tellg()) - buffer.length() - 1;
                // Search for unpaid items
        if (buffer == "NOT PAID")
        {
            std::getline(stream, title);
            std::getline(stream, seller);
            std::getline(stream, price);
                    // Ask user whether to pay for the item
            std::cout << std::endl << "Found unpaid item: " << title << " (" << price << "$)" << std::endl
            << "Seller: " << seller << std::endl;
            choice = glb::getInput("Pay? (Y/N): ", 1, 1, true);
            if (choice == "Y")
            {
                     // Update paid status
                stream.seekg(currentpos);
                stream << "HAS PAID" << std::endl;

                // Update seller file
                std::fstream sellerStream("data/customers/S" + seller + ".txt", std::ios::in | std::ios::out);
                if (!sellerStream) return;

                bool done = false;
                while(std::getline(sellerStream, buffer) && !done)
                {
                    currentpos = int(sellerStream.tellg()) - buffer.length() - 10;
                    if (buffer == title)
                    {
                        sellerStream.seekg(currentpos);
                        sellerStream << "HAS PAID" << std::endl;
                        done = true;
                    }
                }


            }
        }
    }
}

/**
 * Returns a while loop that yield each matching object.
 *
 * @param cb callback       :callback "template"
 * @param std::string query :username to search for, substring ok.
 */
void Customers::search(std::function<void(Customer*)> callback, std::string &query)
{
    if (query.empty())
    {
        return;
    }

    this->loop([&] (auto customer) {
        //checks if query is a substring of username
        if (customer->username().find(query) != std::string::npos) {
            //returns the customer object pointer
            callback(customer);
        }
    });
}

/**
 * Lets an admin create a new user
 */
void Customers::create()
{
    /**
     * username
     */
    std::string username;

    /**
     * Set static user data
     */
    std::cout << "Number of sales / purchases: 0" << std::endl;

    if (_session->getAuthorizationLevel() == ADMIN) {
        std::cout << "User level: CUSTOMER" << std::endl; //TODO: let admin decide
    } else {
        std::cout << "User level: CUSTOMER" << std::endl;
    }

    do {
        if (username.length() > 0) {
            std::cout << "X" << std::endl;
            std::cout << "Error: username already exists!" << std::endl;
            std::cout << "Please try again."<< std::endl;
        }


        username = glb::getInput("Username");

        /**
         * Pretend to be loading..
         */
        std::cout << "Checking if username exists ." << std::flush; glb::fakeLoading();
    } while (_customers->inList(glb::strToChar(username)));
    std::cout << "OK" << std::endl;


    /**
     * Create user
     */
    Customer * customer = new Customer(username);
    _customers->add(customer);   //add user back to list

    /**
     * save customers
     */
    this->save();

    std::cout << "Registered.\n\n";
}

void Customers::save ()
{
    this->_empty();

    std::ofstream stream;
    if (!glb::writeable(_file, stream, true)) {
        return;
    }

    this->loop([&] (Customer * customer) {
        customer->save(stream);
    });

    stream.close();
}

/**
 * @return int, number of customers.
 */
int Customers::length() const
{
    return _customers->noOfElements();
}


/**
 * Loops through all items and sends each instance back as a parameter.
 *
 * @param g_cb_items callback   :Lambda anonymous function that contains the current item instance
 * @param bool add = true       :Whether or not to add the item back into the list
 */
void Customers::loop(std::function<void(Customer*)> callback, bool add)
{
    for (int i = LISTTOOLSTART; i <= this->length(); i++) {
        auto customer = (Customer*) _customers->removeNo(i);
        if (add) {
            _customers->add(customer);
        }
        callback(customer);
    }
}