/**
 * Includes
 */
#include "headers/ListTool2B.h"
#include <string>
#include <iostream>

#include "headers/Category.h"
#include "headers/globals.h"





bool Category::_exists(const std::string title)
{
    bool ret = false;

    this->loop([&ret, &title] (auto category, const int i) {
        if (category->title() == title)
        {
            std::cerr << "Category name exists!" << std::endl;
            ret = true;
        }
    });

    return ret;
}

/*
 * Constructor initializes variables and executes necessary functions
 * Used for both user input and loading from file
 */
Category::Category(int id, std::string path, std::string name)
        : NumElement (id)
{
    _categories = new List(Sorted);
    _title      = name;
    _path       = path + "/" + std::to_string(id);
}

Category::~Category()
{
    delete _categories; // Destroy list
}

/**
 * Saves category information to file and then all its sub categories.
 *
 * @param std::ofstream& stream :output file stream
 */
void Category::save(std::ofstream& stream)
{
    stream << _path << ' ' << this->length() << ' ' << _title << std::endl;

    this->loop([&] (Category* category, const int i) {
        category->save(stream);
    });
}

/**
 * Retrieve all sub categories from file stream
 */
void Category::load(std::ifstream& stream, int children)
{
    std::string path, name;
    int tmpChildren;

    /**
     * Will stop when children reaches; 0 <=> false
     */
    while (children-- && stream >> path >> tmpChildren && stream.ignore() && getline(stream, name))
    {
        Category* category = new Category(this->length(), _path, name);
        category->load(stream, tmpChildren);
        _categories->add(category);
    }
}

int Category::length()
{
    return _categories->noOfElements();
}

// Displays this particular category
void Category::display()
{
    std::cout << std::endl << number << " ->" << _title;
}

// Compares this category's title to the given string parameter
bool Category::compareTitle(std::string title)
{
    return (_title == title);
}

// Creates a sub-category under this category's list
void Category::create ()
{
    if (this->length() == MAX_LIMIT_CATEGORY)  // Stop if there is not more space
    {
        std::cerr << "Maximum amount of categories reached!" << std::endl;
        return;
    }

    std::string name;
    do
    {
        name = glb::getInput("Category name: ", 3, 20);
    } while (this->_exists(name));

    _categories->add(new Category(this->length(), _path, name));
}

Category* Category::getActiveCategory(std::string state)
{
    int prefixLength = 2;
    if (state.length() > prefixLength && std::isdigit(state[prefixLength])) // "/i".length() <=> 2
    {
        state.erase(0, (unsigned long) prefixLength);
        int id = std::stoi(&state[0]);
        auto category = (Category*) _categories->remove(id);
        _categories->add(category);

        return category->getActiveCategory(state);
    }

    return this;
}

std::string Category::path()
{
    return _path;
}

std::string Category::title()
{
    return _title;
}

/**
 * Loops through all items and sends each instance back as a parameter.
 *
 * @param g_cb_items callback   :Lambda anonymous function that contains the current item instance
 * @param bool add = true       :Whether or not to add the item back into the list
 */
void Category::loop(std::function<void(Category*, const int)> callback, bool add)
{
    for (int i = LISTTOOLSTART; i <= this->length(); i++) {
        Category* category = (Category *) _categories->removeNo(i);
        if (add) {
            _categories->add(category); //potential dangling pointer? haven't seen any problems soo far..
        }
        callback(category, i - LISTTOOLSTART);
    }
}