/**
 * Includes
 */
#include <ctime>
#include "headers/Timer4.h"

/**
 * Gets the current time, and manipulates the data inside the ram address given.
 *
 * @param int & day     :Current day        1   -  365
 * @param int & month   :Current month      1   -   12
 * @param int & year    :Current year       2016- xxxx
 * @param int & hour    :Current hour       00  -   23
 * @param int & minute  :Current minute     00  -   59
 *
 * NOTE: This function was modified to conform more to the C++ standard
 */
void Timer4::get(int& day, int& month, int& year, int& hour, int& minute)  {

        // Get current UNIX time
    _timestamp = time(nullptr);
        // Convert to localtime
    _localtime = *localtime( & _timestamp );

    day     = _localtime.tm_mday;
    month   = 1 + _localtime.tm_mon;     // Since tm_mon starts at 0, we add 1 to get the "true" month number
    year    = 1900 + _localtime.tm_year; // Since tm_year returns years since 1900, we add 1900 to get the full year
    hour    = _localtime.tm_hour;
    minute  = _localtime.tm_min;
}