//
// Created by hakkon on 3/20/16.
//



#include "headers/globals.h"

#include <iostream>
#include <cstring>
#include <sstream>
#include <iomanip>
#include <algorithm>

//or just simply..
#include <chrono>
#include <thread>

/**
 * Prints a string with two newlines
 * useful to have less clutter in the code
 */
void glb::print (const std::string& content)
{
    std::cout << content << std::endl;
}

/**
 * Converts a string to a char array
 * Useful when dealing with listtool
 */
char* glb::strToChar(std::string& str)
{
    char* charArray = new char[str.size()+1];
    std::copy(str.begin(), str.end(), charArray);
    charArray[str.size()] = '\0';

    return charArray;
}

/**
 * Clear a terminal window.
 *  Works with linux and windows.
 */
void glb::clear ()
{
    std::cout << std::string(100, '\n');
}


/**
 * Retrieves an input from the terminal.
 *
 * @param std::string   msg         :Message to be displayed when asking for input.
 * @param int           min         :Minimum string length.
 * @param int           max         :Maximum string length.
 * @param bool          uppercase   :Whether or not the returned string should be uppercase.
 * @return std::string
 */
std::string glb::getInput (std::string msg, int min, int max, bool uppercase)
{
    std::string str;
    unsigned long int strLength;

    do {
        std::cout << msg << ":\t";                          // Displays a message to the user.
        std::getline(std::cin, str);                             // Get the terminal input.
        strLength = str.length();                           // save the length for reuse later.

        if (strLength < min || strLength > max) {           // Checks if the restrictions are followed.
            std::cout << "ERROR: The input received was too ";

            if (strLength < min) {                          // Tells the user what exactly the errors is.
                std::cout << "short";
            } else {
                std::cout << "long";
            }

            std::cout << ". Please try again. Press ENTER:" << std::endl;

            std::cin.clear();                               // Clear the cin,
            std::cin.ignore(1000, '\n');                    // and clear the ignore mistakes. Needed for next input!
        }
    } while (strLength < min ||  strLength > max);         // Continue the loop, if the restrictions were not met.



    if (uppercase) {
        for (auto& i : str) {                              // Convert to uppercase if specified.
            i = toupper(i);                                 // Convert each char to uppercase.
        }
    }

    return  str;                                            // Returns an valid input.
}

/**
 * Converts a string to all uppercase
 */
std::string& glb::uppercase (std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

/**
 * Retrieves a number imput from the terminal
 *
 * @param std::string msg   : Message to be shown
 * @param int min           : Minimum value for the number
 * @param int max           : Maximum value for the number
 */
int glb::getInt(std::string msg, int min, int max)
{
    int num;
    bool valid;
        // Display the message
    std::cout << std::endl << msg;
        // Loop until the user has entered a valid number
    do
    {
        valid = true; // Assume valid
        std::cout << " (interval: " << min << '-' << max << "): ";
        std::cin >> num;
        if (!std::cin)  // Not a valid number (contains characters)
        {
            print("Error: input is not a valid number");
            valid = false;
        }
        std::cin.clear();                               // Clear the cin,
        std::cin.ignore(1000, '\n');                    // and clear the ignore mistakes. Needed for next input!
    } while ((num < min || num > max) || !valid);

    return num;
}

/**
 * Takes 5 integers of year, month, day, hours, and minutes and outputs
 * a formatted long integer of the time
 */
long int glb::formatTime(int y, int m, int d, int h, int mi)
{
    long int formatted;
    std::stringstream ss;
        // Format the integers into a stringstream with leading zeroes
    ss << y << std::setw(2) << std::setfill('0') << m << std::setw(2) << std::setfill('0')
    << d << std::setw(2) << std::setfill('0') << h << std::setw(2) << std::setfill('0') << mi;
    std::string timenow = ss.str();
        // Convert the new string to a long int
    formatted = std::stol(timenow);
    return formatted;
}

void glb::write (std::string& file, void callback(std::ofstream &), bool cleanFirst)
{
    std::ofstream stream;
    std::string source = FILE_BASE_PATH + file + FILE_STORAGE_EXTENSION;

    /**
     * Checks if file exists
     */
    if (!std::ifstream(source))
    {
        return; //end function
    }

    /**
     * ofstream for saving files to doctors
     */
    if (cleanFirst) {
        stream.open(source, std::ofstream::out | std::ofstream::trunc); //deletes content
    } else {
        stream.open(file, std::ofstream::out);
    }

    if (!stream)
    {
        throw std::runtime_error("Couldn't open " + source);
    }

    callback(stream); // return ofstream!

    stream.close();
}

void glb::read (std::string& file, std::function<void(std::ifstream&)> callback, bool loop)
{
    std::ifstream stream;
    std::string source = FILE_BASE_PATH + file + FILE_STORAGE_EXTENSION;

    /**
     * Checks if file exists
     */
    if (!std::ifstream(source))
    {
        return; //end function
    }

    /**
     * ofstream for saving files to doctors
     */
        stream.open(source, std::ifstream::in);
    if (!stream) {
        std::cout << "Couldn't open " << source << std::endl;
        return;
    }

    if (loop) {
        while (stream.good())
        {
            /*
            if (stream.peek() == '\n')
            {
                continue;
            }
            */
            callback(stream);
        }
    } else {
        callback(stream); // return stream as a parameter.
    }

    stream.close();
}

bool glb::writeable (std::string& file, std::ofstream & stream, bool cleanFirst)
{
    std::string source = FILE_BASE_PATH + file + FILE_STORAGE_EXTENSION;

    /**
     * ofstream for saving files to doctors
     */
    if (cleanFirst) {
        stream.open(source, std::ofstream::out | std::ofstream::trunc); //deletes content
    } else {
        stream.open(source, std::ofstream::out);
    }

    if (stream.bad()) {
        std::cerr << "Unable to read from " + file + ", in: " + source << std::endl;
        return false;
    }

    return true;
}

bool glb::readable (std::string& file, std::ifstream & stream, bool suppressErrors)
{
    std::string source = FILE_BASE_PATH + file + FILE_STORAGE_EXTENSION;

    stream.open(source, std::ifstream::in);

    if ((stream.rdstate() & std::ifstream::failbit) != 0 && !suppressErrors) {
        std::cerr << source + " could not be opened!" << std::endl;
        return false;
    }

    if (stream.bad() && !suppressErrors) {
        std::cerr << "Unable to read from " + file + ", in: " + source << std::endl;
        return false;
    }

    return true;
}

// Checks if a file exists within the executable directory (and is readable)
bool glb::fileExists(std::string& file)
{
    std::ifstream stream(file);
    return stream.good();
}

// http://stackoverflow.com/a/11276503/3029614
void glb::fakeLoading (int sleep, int dots)
{
    using namespace std::literals::chrono_literals;

    for (int i = 0; i < dots; i++) {
        std::this_thread::sleep_for(300ms);
        std::cout << '.' << std::flush;
    }
}

std::string glb::authorityText (int level)
{
    std::string text;

    if (level == ADMIN) {
        text = "ADMIN";
    } else if (level == CUSTOMER) {
        text = "CUSTOMER";
    }

    return text;
}



Customers*      glb::customers;
Categories*     glb::categories;
Items*          glb::items;

std::string glb::userUpdates = "";

/**
 * Fake mutex
 */
std::string glb::owner_menuState = "";
void glb::setMenuStateOwnership (const bool overwrite)
{
    if (owner_menuState.length() == 0 || overwrite)
    {
        owner_menuState = customers->state();
    }
}

bool glb::owner_menuStateChanged ()
{
    if (glb::owner_menuState.length() > 0 && glb::customers->state().find(glb::owner_menuState) == std::string::npos)
    {
        glb::owner_menuState = "";
        return true;
    }

    return false;
}

bool glb::owner_menuStateFree ()
{
    return glb::owner_menuState.empty();
}

void glb::menu_reload ()
{
    /**
     * Reload the menu by
     */
    std::string tmpState = glb::customers->state();
    glb::customers->previousState();
    glb::customers->updateState( std::to_string(tmpState.back()) );
}

/**
 * Menu list
 */
std::multimap<glb::menu_id, glb::menu_entry, glb::menu_comp> glb::menu;

void glb::addMenuEntry (const glb::menu_id id, const glb::menu_entry content)
{
    menu.insert(std::pair<glb::menu_id, glb::menu_entry>(id, content));
}