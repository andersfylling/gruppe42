/**
 * Header file
 */
#include "headers/Items.h"

/**
 * Includes
 */
#include "headers/constants.h"
#include "headers/globals.h"

#include "headers/Timer4.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <iomanip>



/**
 * Private methods
 */

/**
 * Checks if the list is empty or if an item exists within.
 * A response message with its findings are shown.
 */
bool Items::_exists(int id)
{
    return _items->inList(id);
}

/**
 * Checks if an item exists within the list by
 * comparing a specified string to items title
 */
bool Items::_exists(std::string str)
{
    Item* itemtmp;
            // Iterate through all items
    for (int i = 0; i < _items->noOfElements(); i++)
    {
        itemtmp = (Item*) _items->remove(i);
        if (itemtmp->compare(str))
        {
            _items->add(itemtmp);
            return true;
        }
        _items->add(itemtmp);
    }
    return false;
}

/**
 * Public methods
 */

/**
 * Constructor, initiate a sorted list.
 */
Items::Items()
{
    _items = new List(Sorted);

    _file = "items/";
}

/**
 * Remove the pointer list from memory / destroying it.
 */
Items::~Items()
{
    delete _items;
}

/**
 * Creates an new Item instance.
 */
void Items::create()
{
    Timer4 timer4 = Timer4();   // Initialize variables for time
    int d, m, y, h, mi;

    std::string usr, title, desc;   // Initialize variables
    int startprice, bidinc, porto;
    long int start, end;

    usr = glb::customers->session()->username();

            // Loop until user has typed in an item that doesn't already exist
    do
    {
        title = glb::getInput("Title: ", 3, 30);
        if (_exists(title)) glb::print("Error: this item already exists as an active item.");
    } while (_exists(title));

            // Read all variables from input
    desc = glb::getInput("Description: ", 10, 60);
    startprice = glb::getInt("Starting price: ", 1, MAX_AUCTION_AMOUNT);
    bidinc = glb::getInt("Minimum bid: +", 1, 999);
    porto = glb::getInt("Postage: ", 0, 999);
    y = glb::getInt("End year (YYYY): ", 2016, 2999);
    m = glb::getInt("End month (MM): ", 1, 12);
    d = glb::getInt("End day (DD): ", 1, 30);
    h = glb::getInt("End hour (hh): ", 0, 24);
    mi = glb::getInt("End minutes (mm): ", 0, 60);
    end = glb::formatTime(y, m, d, h, mi);

        // Get current time
    timer4.get(d, m, y, h, mi);
        // Format the time to be stored in integer "start"
    start = glb::formatTime(y, m, d, h, mi);

        // Since we start at id = 0 noOfElements returns the last item ID + 1
    Item* item = new Item(_items->noOfElements(), usr, title, desc, startprice, bidinc, porto, start, end);
    _items->add(item);

    this->save();   // Save the items
}

/**
 * Lets a logged in user submit a bid on an active auction
 */
void Items::bid()
{
            // Check if there are currently active auctions
    if (_items->isEmpty())
    {
        glb::print("Error: no auctions currently active");
    }
            // Get item ID from user
    int id = glb::getInt("Item ID", 0, this->length() - 1);

            // Create a new item instance and add it to the list
    Item* item = (Item*) _items->remove(id);
    item->bid(glb::customers->session()->username());
    _items->add(item);

    this->save();
}

/**
 * Checks to see if any auctions are finished
 * and if so, they are removed from the list and
 * added to the users "K" and "S" files
 */
void Items::updateAuctions()
{
    Item* itemtmp;      // Initialize variables
    Timer4 timer4;
    long int timestamp;
    int d, m, y, h, mi;
    bool ended;

        // Get current time
    timer4.get(d, m, y, h, mi);
    timestamp = glb::formatTime(y, m, d, h, mi);

        // Iterate through every item
    for (int i = 0; i < _items->noOfElements(); i++)
    {
        ended = false;
            // Remove the item and call its updateAuctions()
        itemtmp = (Item*) _items->remove(i);
        itemtmp->updateAuctions(timestamp, ended);
            // If auction ended, delete
        if (ended) delete itemtmp;
            // If not, add it back
        else _items->add(itemtmp);
    }

    this->save();   // Save the items
}

/**
 * Saves everything into a file for storage
 */
void Items::save()
{
    std::ofstream stream;
    if (!glb::writeable(_file, stream, true))
    {
        glb::print("Error: couldn't write to file");
        return;
    }
        // Iterate through every item and call its save function
    this->loop([&stream] (Item* item, const int i) {
        item->save(stream);
    });
}

/**
 * Loads items from a single sub-category into memory
 * According to the category ID parameter
 */
void Items::load(std::string state)
{
    std::string catID = "";

        // Convert the category ID into a string that can be used for the filename
    auto found = state.find_first_of("0123456789/");
    while (found != std::string::npos)
    {
        if (state[found] == '/')
        {
            catID += ".";
        }
        else
        {
            catID += state[found];
        }

        found = state.find_first_of("0123456789/", found+1);
    }

    while (catID.front() == '.')
    {
        catID.erase(catID.begin()); // remove first char
    }
    while (catID.back() == '.')
    {
        catID.pop_back();   // remove last char
    }


    _file += catID;

    std::ifstream stream;
    if (!glb::readable(_file, stream, true)) // suppress errors
    {
        return;
    }

    int id;

        // Read as long as there are more items
    while (stream >> id)
    {
            // Create and add a new item instance
        _items->add(new Item(stream, id));
    }
}

/**
 * Displays a particular item, or all items in the list
 * @param int id    : -1 = display all items
 */
void Items::display(const int id)
{
    if (id != -1)
    {
        Item* item = (Item*) _items->remove(id);
        item->displayDetails();
        _items->add(item);
    }
    else
    {
        _items->displayList();
    }
}

/**
 * Checks the number of items inside the list.
 *
 * @return number of item instances.
 */
int Items::length() const
{
    return _items->noOfElements();
}

/**
 * Loops through all items and sends each instance back as a parameter.
 *
 * @param g_cb_items callback   :Lambda anonymous function that contains the current item instance
 * @param bool add = true       :Whether or not to add the item back into the list
 */
void Items::loop(std::function<void(Item*, const int)> callback, bool add)
{
    for (int i = LISTTOOLSTART; i <= this->length(); i++)
    {
        Item* item = (Item*) _items->removeNo(i);
        if (add) {
            _items->add(item);
        }
        callback(item, i - LISTTOOLSTART);
    }
}