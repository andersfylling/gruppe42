/**
 * Includes
 */
#include <string>
#include <iomanip>

#include "headers/ListTool2B.h"
#include "headers/globals.h"


/**
 * Header file
 */
#include <iostream>


/**
 * Private methods
 */


/**
 * Saves the customer object to file.
 *
 * @param std::ifstream& stream :file stream reference for where to store data.
 */
void Customer::_load(std::ifstream & stream)
{
    stream >> _authority;
    stream >> _purchased;
    stream >> _sold;

    stream.ignore(); // remove new line char

    std::getline(stream, _password);
    std::getline(stream, _name);
    std::getline(stream, _street);

    stream >> _zip;

    stream.ignore(); // remove new line char

    std::getline(stream, _city);
    std::getline(stream, _email);
}

/**
 * Public methods
 */

/**
 * Set the index as the username.
 *
 * @param std::string username  :username of customer
 */
Customer::Customer(std::string username)
        : TextElement (glb::strToChar(username))
{
    _authority  = CUSTOMER;
    _purchased  = 0;
    _sold       = 0;

    _password   = glb::getInput("Password", MIN_LENGTH_PASS, MAX_LENGTH_PASS);
    _name       = glb::getInput("Full name", MIN_LENGTH_NAME, MAX_LENGTH_NAME);
    _street     = glb::getInput("Street address", MIN_LENGTH_STREETADDRESS);
    _zip        = glb::getInput("Zip code", MIN_INT_ZIPCODE);
    _city       = glb::getInput("City", MIN_LENGTH_CITY);
    _email      = glb::getInput("Email", MIN_LENGTH_EMAIL);
}

/**
 * Set the index as the username, and use the load method to extract the rest from file.
 *
 * @param std::string username  :username of customer
 * @param std::ifstream& stream :ifstream reference to read from file
 */
Customer::Customer(std::string username, std::ifstream& stream)
        : TextElement(glb::strToChar(username))
{
    this->_load(stream);
}

/**
 * Displays the customer object on a horizontal line.
 */
void Customer::display() const
{
    int spacing = 18;

    std::cout << std::setw(spacing) << text;
    std::cout << std::setw(spacing) << glb::authorityText(_authority);
    std::cout << std::setw(spacing) << _purchased;
    std::cout << std::setw(spacing) << _sold;
    /* std::cout << std::setw(spacing) << _password; */
    std::cout << std::setw(spacing) << _name;
    std::cout << std::setw(spacing) << _email;
    std::cout << std::setw(spacing) << _street;
    std::cout << std::setw(spacing) << _zip;
    std::cout << std::setw(spacing) << _city;
    std::cout << std::endl;
}

/**
 * Displays a customers purchases and sales, including all feedback
 */
void Customer::displayItems()
{
            // Print purchases, sales and feedback
    printItems("customers/K" + std::string(text), "---------PURCHASES---------");
    printItems("customers/S" + std::string(text), "---------SALES---------");
}

/**
 * Takes filename and a text string as parameter and prints out
 * the contents of a customers "K" or "S" file
 */
void Customer::printItems(std::string file, std::string text)
{
    std::string buffer;
    std::ifstream stream;

    if (!glb::readable(file, stream, true)) {
        glb::print("Error: couldn't read from file");
        return;
    }

    int i = 0;
    std::cout << std::endl;
    glb::print(text);
            // Print all purchases/sales
    while (std::getline(stream, buffer))
    {
        if (buffer == "HAS PAID" || buffer == "NOT PAID")
        {
            i++;
            glb::print("--ITEM NO " + std::to_string(i) + "--");
            glb::print("Paid: " + buffer);
            std::getline(stream, buffer);
            glb::print("Item: " + buffer);
            std::getline(stream, buffer);
            glb::print("User: " + buffer);
            std::getline(stream, buffer);
            glb::print("Paid: " + buffer);
        }
    }
        // Start from the beginning of the stream again
    stream.clear();
    stream.seekg(0, std::ios::beg);

    i = 0;
            // Print all feedback
    while (std::getline(stream, buffer))
    {
        if (buffer == "FEEDBACK")
        {
            i++;
            glb::print("--FEEDBACK NO " + std::to_string(i) + "--");
            std::getline(stream, buffer);
            glb::print("User: " + buffer);
            std::getline(stream, buffer);
            glb::print("Comment: " + buffer);
            std::getline(stream, buffer);
            glb::print("Rating: " + buffer);
        }
    }
}

/**
 * Converts the text index from ListTool to std::string
 *
 * @return std::string :username of customer
 */
std::string Customer::username() const
{
    std::string str;
    str.assign(text);

    return str;
}

/**
 * @return int auth level for customer
 */
int Customer::getAuthorizationLevel() const
{
    return _authority;
}

/**
 * Sets a different authorization level for the customer
 *
 * @param int level :new user level
 */
void Customer::setAuthorization(int level)
{
    _authority = level;
}

/**
 * Compares the parameter with the private user password.
 *
 * @param std::string password
 * @return boolean, true if matched
 */
bool Customer::authenticate(std::string password) const
{
    return (_password.compare(password) == 0);
}

int Customer::newUpdate() const
{
    return _updates;
}

void Customer::addUpdateNotification (int i)
{
    _updates += i;
}

void Customer::removeUpdateNotification (int i)
{
    _updates -= i;
}

/**
 * Stores the customer object to file.
 *
 * @param std::ofstream& stream
 */
bool Customer::save(std::ofstream& stream) //TODO: fix to void
{
    std::string text2;
    text2.assign(text);
    stream  << text2 << std::endl
    << _authority   << std::endl
    << _purchased   << std::endl
    << _sold        << std::endl
    << _password    << std::endl
    << _name        << std::endl
    << _street      << std::endl
    << _zip         << std::endl
    << _city        << std::endl
    << _email       << std::endl;

    return true;
}