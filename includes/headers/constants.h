#ifndef GRUPPE42_CONSTANTS_H
#define GRUPPE42_CONSTANTS_H

/**
 * Includes
 */
#include <map>
#include <string>
#include <limits>


/**
 * Constant variables / structs / etc.
 */
const int ADMIN                     = 4;
const int CUSTOMER                  = 3;
const int GUEST                     = 2;
const int PRIVATE                   = 1;
const int PUBLIC                    = 0;

const int LISTTOOLSTART             = 1;

/**
 * Minimums
 */
const int MIN_LENGTH_NAME           = 4; // ab c :shortest name is 2 chars, and a last name must be specified.
const int MIN_LENGTH_PASS           = 6; // security
const int MIN_LENGTH_EMAIL          = 5; // *@*.*
const int MIN_LENGTH_STREETADDRESS  = 4; // ab 1
const int MIN_LENGTH_CITY           = 2; // ab
const int MIN_INT_ZIPCODE           = 1; // a

/**
 * Maximums
 */
const int MAX_LENGTH_PASS           = 25;
const int MAX_LENGTH_NAME           = 50;
const int MAX_LENGTH_EMAIL          = 50;
const int MAX_LENGTH_STREETADDRESS  = 100;
const int MAX_LIMIT_CATEGORY        = 9;                 // Maximum amount of main-categories.txt and sub-categories.txt under a main-category.
const int MAX_AUCTION_AMOUNT        = std::numeric_limits<int>::max(); // Maximum price of an auction (set to maximum value of type int)


const std::string FILE_BASE_PATH            = "./data/";
const std::string FILE_STORAGE_EXTENSION    = ".txt";

const std::string ROOT_STATE                = "/";
const std::string MENU_SIDES                = "------------------------";
const std::string DISPLAY_STATEMENT         = "------------------------------------------------------------";


const bool DEBUG = false;

#endif //GRUPPE42_CONSTANTS_H
