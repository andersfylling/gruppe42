//
// Created by hakkon on 15.04.16.
//

#ifndef GRUPPE42_BID_H
#define GRUPPE42_BID_H

#include "ListTool2B.h"
#include <string>

class Bid
    : public NumElement
{
    std::string _user;
    long int _timestamp;

public:
    Bid(std::ifstream& stream, std::string, int);
    Bid(int, std::string, long int);
    ~Bid();
    void display();
    void save(std::ofstream&);
    int returnValue();
    std::string returnBidder();
};

#endif //GRUPPE42_BID_H
