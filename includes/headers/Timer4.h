/**
 * Include guard
 */
#ifndef GRUPPE42_TIMER4_H
#define GRUPPE42_TIMER4_H

/**
 * includes
 */
#include <ctime>

/**
 * Class Timer4
 */
class Timer4 {
    time_t  _timestamp;
    tm     _localtime;
public:
    void get(int&, int&, int&, int&, int&);
};


#endif //GRUPPE42_TIMER4_H
