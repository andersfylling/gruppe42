/**
 * Include guard
 */
#ifndef GRUPPE42_CATEGORIES_H
#define GRUPPE42_CATEGORIES_H

/**
 * Includes
 */
//#include <functional> // Callbacks lambdas C++11
#include "ListTool2B.h"
#include "Category.h"
#include <string>
#include <functional>

/**
 * Class Categories
 */
class Categories
{
    List*       _categories;
    std::string _file;

    bool _exists(const std::string);
public:
    Categories                      ();                         // Constructor
    ~Categories                     ();                         // Destructor

    Category*   getActiveCategory   (std::string);
    void        load                ();
    void        save                ();
    int         length              ()                  const;  // Number of item instances
    void        create              ();
    void        createSub           ();
    void        display             ();
    void        surf                ();

    void loop                       (std::function<void(Category*, const int)>, bool = true);
};


#endif //GRUPPE42_CATEGORIES_H
