//
// Created by andes on 14.03.16.
//

#ifndef GRUPPE42_CATEGORY_H
#define GRUPPE42_CATEGORY_H

#include "ListTool2B.h"
#include <string>
#include <functional>

/**
 * Forward declaration for cb
 */
class Category;

//typedef std::function<void(Category*, const int)> cb;

class Category
        : public NumElement
{
    List * _categories;
    std::string _title;
    std::string _file;
    std::string _path;

    bool _exists(const std::string);
public:
    Category    (int id, std::string, std::string);
    ~Category   ();

    void display();

    Category*   getActiveCategory   (std::string);
    int  length      ();                                     // Number of item instances
    bool compareTitle(std::string);
    void create();
    void save(std::ofstream&);
    void load(std::ifstream&, int);
    std::string select();

    void surf(std::string);
    std::string path();
    std::string title();

    void loop (std::function<void(Category*, const int)>, bool = true);
};


#endif //GRUPPE42_CATEGORY_H
