#ifndef GRUPPE42_CUSTOMER_H
#define GRUPPE42_CUSTOMER_H

/**
 * Includes
 */
#include "ListTool2B.h"
#include "constants.h"
#include <string>
#include <fstream>


class Customer
        : public TextElement
{
    int _authority;
    int _purchased;
    int _sold;
    int _updates;

    std::string _password;
    std::string _name;
    std::string _street;
    std::string _zip;
    std::string _city;
    std::string _email;

    void        _load                   (std::ifstream&);               // Loads in customer data from file
public:
    Customer                            (std::string, std::ifstream&);  // Constructor
    Customer                            (std::string);                  // Constructor

    void        display                 () const;                       // ListTool2B
    void        displayItems            ();                             // Display customers items

    std::string username                () const;                       // Returns username
    bool        authenticate            (std::string) const;            // Compare passwords
    int         getAuthorizationLevel   () const;                       // Gets users authorization level
    void        setAuthorization        (int);                          // Sets new authorization level
    int         newUpdate               () const;
    void        addUpdateNotification   (int = 1);
    void        removeUpdateNotification(int = 1);
    void        printItems              (std::string, std::string);

    bool        save                    (std::ofstream&);               // Saves the customer to file
};


#endif //GRUPPE42_CUSTOMER_H