/**
 * Include guard
 */
#ifndef GRUPPE42_CUSTOMERS_H
#define GRUPPE42_CUSTOMERS_H

/**
 * Includes
 */
#include "ListTool2B.h"
#include "Customer.h"
#include <string>
#include <functional>

/**
 * Class Customers
 */
class Customers
{
    List *      _customers;                         // List of all registered customers
    Customer *  _session;                           // Current logged in user
    std::string _file;

    std::string _state;

    void _empty         ()                          const;
    void _exists        (int&)                      const;
    void _exists        (std::string)               const;
    void _load          ();
public:
    Customers           ();                                 // Constructor
    ~Customers          ();                                 // Destructor

    bool authenticated  ()                          const;  // checks if user is logged in
    void deauthenticate ();                                 // logs out
    void setAuthority   ();
    Customer * session  ()                          const;  //returns username of session, or ""
    bool authenticate   (std::string, std::string);         // Log in a user
    void create         ();                                 // Registers a new user
    int  length         ()                          const;  // number of users
    bool remove         (int);                              // Removes a user by index
    bool remove         (std::string);                      // Removes a user by username
    void save           ();                                 // Load data from files
    void search(std::function<void(Customer *)>, std::string &);

    std::string state   ()                          const;
    void updateState    (std::string);
    void previousState  (int = 1);
    void payForAuction  (std::string user);

    void loop           (std::function<void(Customer*)>, bool = true);          // Loop that has a callback parameter for each element
};


#endif //GRUPPE42_CUSTOMERS_H
