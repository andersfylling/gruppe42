//
// Created by andes on 15.03.16.
//

#ifndef GRUPPE42_GLOBALS_H
#define GRUPPE42_GLOBALS_H

/**
 * includes
 */
#include "Customers.h"
#include "Categories.h"
#include "Items.h"

#include <string>
#include <fstream>
#include "constants.h"


namespace glb
{
    /**
     * Global functions
     */
    extern void                         print(const std::string& = "");
    extern void                         clear();
    extern char *                       strToChar(std::string&);
    extern std::string                  getInput(std::string, int = 0, int = 150, bool = false);
    extern int                          getInt(std::string, int, int);
    extern long int                     formatTime(int, int, int, int, int);
    extern std::string&                 uppercase(std::string&);

    extern void write                   (std::string&, void(std::ofstream&), bool = false);
    extern void read                    (std::string&, std::function<void(std::ifstream&)>, bool = true);
    extern bool writeable               (std::string&, std::ofstream &, bool = false);
    extern bool readable                (std::string&, std::ifstream &, bool = false);
    extern bool fileExists              (std::string&);

    extern void fakeLoading             (int = 300000, int = 3);
    extern std::string authorityText    (int = CUSTOMER);

    //typedef void (* method_ptr)         ();


    extern void categorySurfing         (); // AS


    //extern struct Session;

    extern Customers*   customers;
    extern Categories*  categories;
    extern Items*       items;

    extern std::string userUpdates;

    /**
     * transfer ownership to another state. Works kinda like a mutex.
     */
    extern std::string owner_menuState;
    extern void setMenuStateOwnership   (const bool = false);
    extern bool owner_menuStateChanged  ();
    extern bool owner_menuStateFree     ();
    extern void menu_reload             ();

    /**
     * Entry data content
     */

    enum menu_type
    {
        enum_item, enum_category, enum_function, enum_path
    };

    struct menu_id
    {
        std::string command;
        std::string state;
    };

    struct menu_entry
    {
        std::string             description = "Link description";
        int                     minAuth     = ADMIN;
        int                     maxAuth     = GUEST;
        std::function<void()>   fptr        = nullptr; // void (*fptr)()
        menu_type               race        = enum_function; //default
        int                     timeout     = -1;
    };

    struct menu_comp
    {
        bool operator()(const menu_id& a, const menu_id& b) {
            return std::tie(a.command, a.state) < std::tie(b.command, b.state);
        }
    };


    /**
     * Menu list
     */
    extern std::multimap<menu_id, menu_entry, menu_comp> menu;

    extern void addMenuEntry(menu_id, menu_entry);

    /**
     * Declarations for function pointers. Is only used for menu menu.
     */

    /**
     * Menu functions requested by the project task.
     */
    extern void registerCustomer           (); // KR
    extern void loginCustomer              (); // KL
    extern void logoutCustomer             (); // KU
    extern void displayCustomerItems       (); // KS
    extern void payForAuction              (); // KB
    extern void giveAuctionFeedback        (); // KF
    extern void setAuthorityLevel          (); // KE
    extern void createCategory             (); // AHU
    extern void displayActiveAuctions      (); // A
    extern void displayInfoAboutItem       (); // E
    extern void bidOnItem                  (); // B
    extern void addNewItem                 (); // L
    // nullptr                          // Q

    /**
     * custom / additional functions
     */
    extern void searchForCustomers         ();
    extern void addItemsToMenu             (std::function<void()>, std::string, const int);
    extern void updateLoop                 ();
    extern void updateAuctions             ();
}


#endif //GRUPPE42_GLOBALS_H