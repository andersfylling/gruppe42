/**
 * Include guard
 */
#ifndef GRUPPE42_ITEMS_H
#define GRUPPE42_ITEMS_H

/**
 * Includes
 */
#include "ListTool2B.h"
#include <functional>       //lambda c++11
#include "Item.h"


/**
 * Class Categories
 */
class Items
{
    List *          _items;                                 // Item instances
    std::string     _file;

    bool _exists    (int);
    bool _exists    (std::string);
public:
    Items           ();                             // Constructor
    ~Items          ();                             // Destructor

    void create     ();                             // Registers a new user
    Item * remove     (int);                        // Removes a user by index
    bool destroy    (int);                        // Destroy a user by index
    void save       ();                             // Load data from files
    void load       (std::string);                             // Store data to files
    int  length     () const;                             // Number of item instances
    void display    (const int = -1);
    void bid        ();
    void updateAuctions  ();

    void loop       (std::function<void(Item*, const int)>, bool = true);// Loop that has a callback parameter for each element
    void extendLifetime (int&);
};


#endif //GRUPPE42_ITEMS_H
