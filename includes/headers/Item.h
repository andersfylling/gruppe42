#ifndef GRUPPE42_ITEM_H
#define GRUPPE42_ITEM_H

#include "ListTool2B.h"
#include <string>

class Item
        : public NumElement
{
    List* _bids;
    std::string _seller;
    std::string _title;
    std::string _description;
    long int _start;
    long int _end;
    int _startingPrice;
    int _bidIncrement;
    int _porto;



public:
    Item (int, std::string, std::string, std::string, int, int, int, long int, long int);
    Item (std::ifstream&, int id);
    ~Item();

    bool compare (int id);
    bool compare (std::string);
    void display();
    void save(std::ofstream&);
    void read(std::ifstream&, int);
    std::string title() const;

    void displayDetails();
    void bid(std::string);
    void updateAuctions(long int, bool&);
};


#endif //GRUPPE42_ITEM_H
