#include <fstream>
#include <iostream>
#include "headers/Bid.h"


Bid::Bid(std::ifstream &stream, std::string user, int price)
        :NumElement (price)
{
    std::string buffer;
    _user = user;

    std::getline(stream, buffer);
    _timestamp = std::stol(buffer);
}

Bid::Bid(int price, std::string user, long int timestamp)
    :NumElement (price)
{
    _user = user;
    _timestamp = timestamp;
}

Bid::~Bid()
{

}

void Bid::display()
{
    std::cout << std::endl << "User: " << _user;
    std::cout << std::endl << "Amount: " << number;
    std::cout << std::endl << "Time: " << _timestamp;
}

void Bid::save(std::ofstream& stream)
{
    stream << _user << std::endl
    << number << std::endl
    << _timestamp << std::endl;
}

int Bid::returnValue()
{
    return number;
}

std::string Bid::returnBidder()
{
    return _user;
}