//
// Created by andes on 14.03.16.
//


/**
 * Header file
 */
#include "headers/Categories.h"

/**
 * Includes
 */
#include <iostream>
#include "headers/constants.h"
#include "headers/globals.h"


bool Categories::_exists(const std::string title)
{
    bool ret = false;

    this->loop([&ret, &title] (auto category, const int i) {
        if (category->title() == title)
        {
            std::cerr << "Category name exists!" << std::endl;
            ret = true;
        }
    });

    return ret;
}

/**
 * Constructor create a new sorted List instance
 */
Categories::Categories()
{
    _categories = new List(Sorted);
    _file       = "categories";

    this->load();   // Load categories from file into memory
}

/**
 * Destructor
 */
Categories::~Categories()
{
    delete _categories; // Destroy all elements
}

/**
 * Initiates a new category and adds it to the list
 *
 * @return true if the new category was added
 */

// Loads the main categories from file
void Categories::load()
{
    std::ifstream stream;
    if (!glb::readable(_file, stream, true)) // suppress errors
    {
        return;
    }

    /**
     * continue until end of file.
     */
    std::string path, name;
    int children;
    while (stream >> path >> children && stream.ignore() && getline(stream, name))
    {
        auto category = new Category(this->length(), "", name); //ignore path for main categories
        category->load(stream, children);
        _categories->add(category);
    }
}

/**
 * Save current list existing within memory.
 */
void Categories::save()
{
    std::ofstream stream;
    if (!glb::writeable(_file, stream, true)) // clear file content before saving!!
    {
        return;
    }

    this->loop([&stream] (auto category, const int i) {
        category->save(stream);
    });
}

/**
 * Returns the length of the private List object
 *
 * @return integer equal the number of elements stored
 */
int Categories::length() const
{
    return _categories->noOfElements();
}

//Creates a new main category from user input: name
void Categories::create()
{
    if (this->length() == MAX_LIMIT_CATEGORY)  // Stop if there is not more space
    {
        std::cerr << "Maximum amount of categories reached!" << std::endl;
        return;
    }

    std::string name;
    do  // Loop input until the name is not a duplicate of an existing category
    {
        name = glb::getInput("Category name: ", 3, 20);
    } while (this->_exists(name));

    _categories->add(new Category(this->length(), "", name));   // Add it to the categories list

    this->save();   // Save the category to file
}

// Displays all categories
void Categories::display()
{
    _categories->displayList();
}

/**
 * Finds out which category the user is currently in.
 *
 * @param std::string state :active user state
 * @return a pointer to the active category. nullptr if it regards the list in categories
 */
Category* Categories::getActiveCategory(std::string state)
{
    std::string prefix = "/AS/";
    if ( state.length() > prefix.length() && std::isdigit(state[prefix.length()]) )
    {
        state.erase(0, prefix.length());
        int id = state[0] - '0'; //char to int
        auto category = (Category*) _categories->remove(id);
        _categories->add(category);
        return category->getActiveCategory(state); //dangling pointer?
    }

    return nullptr;
}


/**
 * Lets the user go through categories in a more natural way.
 *  Categories are loaded into memory on request.
 */
void Categories::surf()
{
    // Take ownership
    glb::setMenuStateOwnership();

    // Get the current active / accessed category
    auto activeCategory = this->getActiveCategory(glb::customers->state());

    // Print out the menu options, if its nullptr then it regards the customers list here
    if (activeCategory == nullptr) {
        this->loop([] (auto category, const int i) {
            auto search = glb::menu.find({std::to_string(i), "/AS"});
            if (search == glb::menu.end()) {
                glb::addMenuEntry(
                        {std::to_string(i), "/AS"},
                        {category->title(), PUBLIC, ADMIN, glb::categorySurfing, glb::enum_category}
                );
            }
        });
    }
    else
    {
        activeCategory->loop([] (auto category, const int i) {
            /**
             * Since this is only categories from the desired category's list,
             *  it is needed to remove the two last chars from their string.
             *  The two last characters are the ones that defines their own
             *  unique state id.
             */
            std::string path = category->path();
            path.erase(path.length() - 2, 2);

            /**
             * Create a iterator object from the menu map
             *  and check if its value is too high.
             *
             *  search == menu.end() :no match found, this is so the same elements wont be added twice
             */
            auto search = glb::menu.find({std::to_string(i), "/AS" + path});
            if (search == glb::menu.end())
            {
                /**
                 * If the menu entry doesn't exist, it is added.
                 */
                glb::addMenuEntry(
                        {std::to_string(i), "/AS" + path},
                        {category->title(), PUBLIC, ADMIN, glb::categorySurfing, glb::enum_category}
                );
            }
        });
    }
}


/**
 * Loops through all items and sends each instance back as a parameter.
 *
 * @param void (auto, const int) callback   :Lambda anonymous function that contains the current item instance
 * @param bool add = true                   :Whether or not to add the item back into the list
 */
void Categories::loop(std::function<void(Category*, const int)> callback, bool add)
{
    for (int i = LISTTOOLSTART; i <= this->length(); i++) {
        Category* category = (Category *) _categories->removeNo(i);
        if (add) {
            _categories->add(category);
        }
        callback(category, i - LISTTOOLSTART);
    }
}