#FILE STRUCTURE

 * std::string "PAID" / "NOT PAID"     
 * std::string ItemName                
 * std::string Seller/Buyer            
 * int  FinalPrice              
 * *std::string "PAID" / "NOT PAID"*  
 * *std::string ItemName*              
 * *std::string Seller/Buyer*          
 * *int         FinalPrice*            
 * std::string "PAID" / "NOT PAID"     
 * std::string ItemName              
 * std::string Seller/Buyer           
 * int         FinalPrice             
 * *std::string "PAID" / "NOT PAID"*  
 * *std::string ItemName*             
 * *std::string Seller/Buyer*         
 * *int         FinalPrice*           
 * std::string "PAID" / "NOT PAID"     
 * std::string ItemName               
 * std::string Seller/Buyer            
 * int         FinalPrice              
*[...]*


 * FEEDBACK
 * std::string Username   
 * std::string Comment     
 * std::string Grade      
 * *std::string Username*  
 * *std::string Comment*  
 * *std::string Grade*    
 * std::string Username   
 * std::string Comment     
 * std::string Grade      
 * *std::string Username*  
 * *std::string Comment*   
 * *std::string Grade*     
 * std::string Username    
 * std::string Comment     
 * std::string Grade       
*[...]*


#READING FILE
#####Reading won auctions
```cpp
std::string kFile = "customers/K" + glb::customers->session()->username();
glb::read(kFile, [] (auto stream) {

    /**
    * check for a auction.
    *
    * The First line of each auction is "HAS PAID" or "NOT PAID"
    *  therefore the program check if the line " PAID" exists as this
    *  is unique due to its uppercase value.
    *
    *  the customer must always have paid before the can comment.
    */
    std::string status;
    while (std::getline(stream, status))
    {
    if (status == "HAS PAID")
        {
            break;
        }
    }

    /**
     * Get content
     */
    std::string status;                    // "PAID" or "NOT PAID"
    std::getline(stream, status);

    std::string item;                       // "some item title"
    std::getline(stream, item);

    std::string seller;                     // the seller (mark this is the K file!)
    std::getline(stream, seller);


    std::string finalPrice;                // The price decided
    std::getline(stream, finalPrice);

    /**
     * Only let the customer send feedback if he
     *  or she has actually paid for the item
     */
    if (status == "PAID")
    {
        /* Do something with the data */
    }
});
```