FILE STRUCTURE
<ID>
<Seller Name>
<Item Name>
<Description>
<Start>
<End>
<Start Price>
<Bid Interval>
<Shipping Price>
<Bid 1 Username>
<Price>
<Timestamp>
<Bid 2 Username>
<Price>
<Timestamp>
etc...

NOTE: There does not need to be Bid entries, all items are separated by an empty line, and the code
checks if the next line is empty (if it needs to read bids).