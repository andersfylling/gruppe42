# Developers #

###Anders Øen Fylling###
#####anders@nordic.email#####
#####902 18 202#####


###Håkon Legernæs###
#####hakon.legernas@hig.no#####
#####481 27 703#####


# Resources #

 * http://www.stroustrup.com/glossary.html
 * https://www.jetbrains.com/clion
 * https://gcc.gnu.org/
 * https://cmake.org/
 
# Software versions #
 * clion  2016.1
 * gcc    5.2.1
 * CMake  3.5.1
 * GDB    7.8
 * C++14
 